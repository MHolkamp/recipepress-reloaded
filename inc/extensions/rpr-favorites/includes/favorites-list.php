<?php
/**
 * The HTML markup of our favorite recipes
 *
 * @package Recipepress
 */

?>

<div id="rpr-favorites-list"></div>

<script id="favorites-list" type="text/mvl">
	<div class="rpr-favorite-container">
		<div If={recipes.length}>
			<div class="rpr-favorite-item" data-id={$recipe.id} For={$recipe in recipes.reverse()}>
				<div class="rpr-favorite-remove">
					<i @click={removeFavorite($recipe.id)} class="rpr-icon icon-trash" title="Remove from list"></i>
				</div>
				<div class="rpr-favorite-email">
					<i @click={emailFavorite($recipe)} class="rpr-icon icon-mail-alt" title="Email this recipe"></i>
				</div>
				<div class="rpr-favorite-thumbnail">
					<a href={$recipe.url} rel="noopener">
						<img src={$recipe.thumbnail} />
					</a>
				</div>
					<div class="rpr-favorite-info">
						<a href={$recipe.url} rel="noopener">
							<h2>{$recipe.title}</h2>
						</a>
						<p>{$recipe.description}</p>
						<span>Added on {$recipe.date}</span>
					</div>
			</div>
		</div>
		<div Else>
			<div class="rpr-favorite-empty">
				<p>You have not added any recipes to your favorite list.</p>
			</div>
		</div>
	</div>
</script>
