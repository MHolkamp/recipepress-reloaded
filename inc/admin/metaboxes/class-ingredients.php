<?php

namespace Recipepress\Inc\Admin\Metaboxes;

use Recipepress\Inc\Core\Options;
use Recipepress\Inc\Common\Abstracts\Metadata;

/**
 * Saving the instructions meta information.
 *
 * @package    Recipepress
 * @subpackage Recipepress/inc/admin/metaboxes
 * @author     Kemory Grubb <kemory@wzymedia.com>
 */
class Ingredients extends Metadata {

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since   1.0.0
	 *
	 * @param   string $plugin_name The ID of this plugin.
	 * @param   string $version The current version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		parent::__construct( $plugin_name, $version, 'rpr_ingredients_metabox', __DIR__, true, true );
	}

	/**
	 * Add a metabox for the recipe ingredients.
	 *
	 * We are removing the default ingredients taxonomy metabox
	 * and replacing it with our own.
	 *
	 * @since 1.0.0
	 *
	 * @uses  add_meta_box
	 * @return void
	 */
	public function add_metabox() {
		// Remove default ingredient taxonomy metabox from side.
		remove_meta_box( 'tagsdiv-rpr_ingredient', 'rpr_recipe', 'side' );

		add_meta_box(
			$this->metabox_id,
			__( 'Ingredients', 'recipepress-reloaded' ),
			array( $this, 'render_metabox' ),
			'rpr_recipe',
			'normal',
			'high'
		);
	}

	/**
	 * Should we display this metabox.
	 *
	 * @since 1.0.0
	 * @return bool
	 */
	protected function display_metabox() {
		return true;
	}

	/**
	 * Get the list of pre-configured ingredient unit list entered
	 * in our setting page.
	 *
	 * @since 1.0.0
	 *
	 * @param string $selected The current selected ingredient unit.
	 *
	 * @return string
	 */
	public function get_the_ingredient_unit_selection( $selected = null ) {

		$units = explode( ',', Options::get_option( 'rpr_ingredient_unit_list', array() ) );
		$out   = '';

		foreach ( $units as $key => $unit ) {
			$out .= '<option value="' . $unit . '"';
			if ( $unit === $selected ) {
				$out .= ' selected="selected" ';
			}
			$out .= '>' . $unit . '</option>' . "\n";
		}
		if ( ! in_array( $selected, $units, true ) ) {
			$out .= '<option value="' . esc_attr( $selected ) . '"  selected="selected" >' . esc_attr( $selected ) . '</option>';
		}

		return $out;
	}

	/**
	 * Check the presence of, sanitizes then saves the ingredients.
	 *
	 * @since 1.0.0
	 *
	 * @uses  update_post_meta()
	 * @uses  wp_set_post_terms()
	 * @uses  sanitize_text_field()
	 *
	 * @param int      $recipe_id The post ID of the recipe post.
	 * @param array    $data      The data passed from the $_POST request.
	 * @param \WP_Post $recipe    The recipe object this data is being saved to.
	 *
	 * @return bool|int|\WP_Error
	 */
	public function save_metabox_metadata( $recipe_id, $data, $recipe ) {

		if ( ! $this->check_nonce( $data ) ) {
			return false;
		}

		$ingredients = isset( $data['rpr_recipe_ingredients'] ) ? $data['rpr_recipe_ingredients'] : array();
		// A new array to contain all non-empty line from the form.
		$non_empty = array();
		// An array of all ingredient term_ids to create a relation to the recipe.
		$ingredient_tax = array();

		foreach ( (array) $ingredients as $ing ) {
			// Check if we have an ingredients group or an ingredient.
			if ( isset( $ing['grouptitle'] ) ) {
				// we have an ingredient group title line
				// We do nothing and will save this line as is to the recipe metadata.
				if ( '' !== $ing['grouptitle'] ) {
					$non_empty[] = $ing;
				}
			} else {
				// we have a single ingredient line.
				if ( '' !== $ing['ingredient'] ) {
					// We need to find the term_id of the ingredient and add a taxonomy relation to the recipe.
					$term = term_exists( $ing['ingredient'], 'rpr_ingredient' );

					if ( 0 === $term || null === $term ) {
						// Ingredient is not an existing term, create it.
						$term = wp_insert_term( sanitize_text_field( $ing['ingredient'] ), 'rpr_ingredient' );
					}

					if ( is_wp_error( $term ) ) {
						return new \WP_Error( 'invalid_term', 'Invalid ingredient term: ' . $ing['ingredient'] );
					}

					// Now we have a valid term id!
					$term_id = (int) $term['term_id'];

					// This means it's a new ingredient being created.
					if ( '' === get_term_meta( $term_id, 'ingredient_custom_meta', true ) ) {
						update_term_meta( $term_id, 'ingredient_custom_meta', array( 'use_in_listings' => '1' ) );
					}

					// Set it to the ingredient array.
					$ing['ingredient_id'] = $term_id;
					// add it to the taxonomy list.
					$ingredient_tax[] = $term_id;
					// Add it to the save list.
					$non_empty[] = array_map( 'sanitize_text_field', $ing );
				}
			}
		}
		// Save the recipe <-> ingredient taxonomy relationship.
		wp_set_post_terms( $recipe_id, $ingredient_tax, 'rpr_ingredient' );
		// Save the new metadata array.
		update_post_meta( $recipe_id, 'rpr_recipe_ingredients', $non_empty );

		return $recipe_id;
	}

}
