<?php
/**
 * The ingredient metabox view of the plugin.
 *
 * @since 1.0.0
 *
 * @var object $recipe
 * @var object $this Recipepress\Inc\Admin\Metaboxes\Ingredients
 *
 * @package    recipepress-reloaded
 * @subpackage recipepress-reloaded/admin/views
 */

use Recipepress\Inc\Core\Options;

$this->create_nonce();
?>

<script type="text/javascript">
	<?php
		$ingredients = get_terms(
			'rpr_ingredient',
			array(
				'orderby' => 'name',
				'order'   => 'ASC',
			)
		);
		$ing_list    = array();
		foreach ( $ingredients as $ing ) {
			$ing_list[] = $ing->name;
		}
	?>

	var hay_stack = <?php echo wp_json_encode( $ing_list ); ?>;
	jQuery(document).on('focusin', '.rpr-ing-name-input', function () {
		jQuery(this).autocomplete({
			source: hay_stack,
			minLength: 2,
			autoFocus: true
		});
	});
	jQuery(document).on('focusout', '.rpr-ing-name-input', function () {
		jQuery(this).autocomplete('destroy');
	});
</script>

<table width="100%" cellspacing="5" class="rpr-metabox-table ingredients" id="recipe-ingredients">
	<thead>
	<tr>
		<th class="rpr-ing-sort">
			<div class="dashicons dashicons-sort"></div>
		</th>
		<th class="rpr-ing-amount"><?php esc_attr_e( 'Amount', 'recipepress-reloaded' ); ?></th>
		<th class="rpr-ing-unit"><?php esc_attr_e( 'Unit', 'recipepress-reloaded' ); ?></th>
		<th class="rpr-ing-ingredient"><?php esc_attr_e( 'Ingredient', 'recipepress-reloaded' ); ?></th>
		<th class="rpr-ing-note"><?php esc_attr_e( 'Note', 'recipepress-reloaded' ); ?></th>
		<th class="rpr-ing-link">
			<div class=""></div>
		</th>
		<th class="rpr-ing-del">
			<div class=""></div>
		</th>
	</tr>
	</thead>

	<tbody>
	<!-- hidden group name row to copy heading lines from -->
	<tr class="ingredient-group-stub rpr-hidden">
		<td class="rpr-ing-sort">
			<div class="sort-handle dashicons dashicons-sort"></div>
			<input type="hidden" name="rpr_recipe_ingredients[0][sort]" class="ingredients_sort"
				id="ingredients_sort_0"/>
		</td>
		<td colspan="5" class="rpr-ing-group">
			<label for="rpr_recipe_ingredients[0][grouptitle]" class="ingredient-group-label screen-reader-text">
				<?php esc_attr_e( 'Ingredient Group Title', 'recipepress-reloaded' ); ?>
			</label>
			<input type="text" class="ingredient-group-label" name="rpr_recipe_ingredients[0][grouptitle]"
				id="ingredients_grouptitle_0" placeholder="<?php esc_attr_e( 'Ingredient group title', 'recipepress-reloaded' ); ?>" />
		</td>
		<td class="rpr-ing-add">
			<button class="rpr-ing-add-row button dashicons dashicons-plus" data-type="rpr_ingredient"
					title="<?php esc_attr_e( 'Add group title', 'recipepress-reloaded' ); ?>"></button>
		</td>
		<td class="rpr-ing-del">
			<button class="rpr-ing-remove-row button dashicons dashicons-no" data-type="rpr_ingredient"
				title="<?php esc_attr_e( 'Remove group title', 'recipepress-reloaded' ); ?>"></button>
		</td>
	</tr>

	<!-- Existing ingredient rows -->
<?php
	$ingredients = get_post_meta( $recipe->ID, 'rpr_recipe_ingredients', true );
	$i           = 1;
?>

<?php
	if ( is_array( $ingredients ) ) {
		foreach ( $ingredients as $ing ) {

			$ing_custom_meta = ! empty( $ing['ingredient_id'] ) ? get_term_meta( $ing['ingredient_id'], 'ingredient_custom_meta', true ) : null;
			$global_link     = ! empty( $ing_custom_meta['link'] ) ? $ing_custom_meta['link'] : '';
			$has_link        = '';

			// Check if we have a ingredients group or a ingredient.
			if ( isset( $ing['grouptitle'] ) ) {
				// If we have a ingredient group title line add a group heading line.
				if ( '' !== $ing['grouptitle'] ) {
					?>
					<tr class="ingredient-group">
						<td class="rpr-ing-sort">
							<div class="sort-handle dashicons dashicons-sort"></div>
							<input type="hidden" name="rpr_recipe_ingredients[<?php echo $i; ?>][sort]"
								   class="ingredients_sort" id="ingredients_sort_<?php echo $i; ?>"
                                   value="<?php echo $i; ?>" />
						</td>
						<td colspan="5" class="rpr-ing-group">
							<label for="rpr_recipe_ingredients[<?php echo $i; ?>][grouptitle]"
								   class="ingredient-group-label screen-reader-text">
								<?php esc_attr_e( 'Ingredient Group Title', 'recipepress-reloaded' ); ?>:
							</label>
							<input type="text" name="rpr_recipe_ingredients[<?php echo $i; ?>][grouptitle]"
								   class="ingredients_grouptitle" id="ingredients_grouptitle_<?php echo $i; ?>"
								   value="<?php echo $ing['grouptitle']; ?>"/>
						</td>
						<td class="rpr-ing-add">
							<button class="rpr-ing-add-row button dashicons dashicons-plus" data-type="rpr_ingredient"
								title="<?php esc_attr_e( 'Add group title', 'recipepress-reloaded' ); ?>"></button>
						</td>
						<td class="rpr-ing-del">
							<button class="rpr-ing-remove-row button dashicons dashicons-no" data-type="rpr_ingredient"
							   title="<?php esc_attr_e( 'Remove group title', 'recipepress-reloaded' ); ?>"></button>
						</td>
					</tr>
					<?php
				}
			} else {
				// We have a single ingredient line, get the term name from the term_id just in case it has changed.
				$term = get_term( $ing['ingredient_id'], 'rpr_ingredient' );
				if ( null !== $term && ! is_wp_error( $term ) ) {
					$ing['ingredient'] = $term->name;
				}
				if ( '' !== $ing['link'] || '' !== $global_link ) {
					$has_link = 'has-link';
				}
				// Add single ingredient line.
				?>
				<tr class="rpr-ing-row row-<?php echo $i; ?>">
				<td class="rpr-ing-sort">
					<div class="sort-handle dashicons dashicons-sort"></div>
					<input type="hidden" name="rpr_recipe_ingredients[<?php echo $i; ?>][sort]"
						   class="ingredients_sort" id="ingredients_sort_<?php echo $i; ?>"
						   value="<?php echo $i; ?>"/>
				</td>
				<td class="rpr-ing-amount">
					<input type="text" name="rpr_recipe_ingredients[<?php echo $i; ?>][amount]"
						   class="ingredients_amount" id="ingredients_amount_<?php echo $i; ?>"
						   value="<?php echo $ing['amount']; ?>"/>
				</td>
				<td class="rpr-ing-unit">
					<?php
					// If ingredient list should be used.
					if ( Options::get_option( 'rpr_use_ingredient_unit_list' ) ) {
						?>
						<select name="rpr_recipe_ingredients[<?php echo $i; ?>][unit]"
								class="ingredients_unit" id="ingredient_unit_<?php echo $i; ?>">
							<?php echo $this->get_the_ingredient_unit_selection( $ing['unit'] ); ?>
						</select>
						<?php
						// if not we just use a standard input field
					} else {
						?>
						<input type="text" name="rpr_recipe_ingredients[<?php echo $i; ?>][unit]"
							   class="ingredients_unit" id="ingredient_unit_<?php echo $i; ?>"
							   value="<?php echo $ing['unit']; ?>"/>
					<?php } ?>
				</td>
				<td class="rpr-ing-name">
					<input type="text" class="rpr-ing-name-input"
						   data-ingredient-id="<?php echo $ing['ingredient_id']; ?>"
						   name="rpr_recipe_ingredients[<?php echo $i; ?>][ingredient]" class="ingredients_name"
						   id="ingredients_<?php echo $i; ?>" value="<?php echo $ing['ingredient']; ?>"/>
				</td>
				<td class="rpr-ing-note">
					<input type="text" name="rpr_recipe_ingredients[<?php echo $i; ?>][notes]"
						   class="ingredients_notes" id="ingredient_notes_<?php echo $i; ?>"
						   value="<?php echo $ing['notes']; ?>"/>
				</td>
				<td class="rpr-ing-link">
				<input name="rpr_recipe_ingredients[<?php echo $i; ?>][link]"
					   class="rpr_recipe_ingredients_link" type="hidden"
					   id="ingredient_link_<?php echo $i; ?>" value="<?php echo ( '' !== $ing['link'] ) ? $ing['link'] : $global_link; ?>"/>
				<input name="rpr_recipe_ingredients[<?php echo $i; ?>][target]"
					   class="rpr_recipe_ingredients_target" type="hidden"
					   id="ingredient_target_<?php echo $i; ?>" value="<?php echo isset( $ing['target'] ) ? $ing['target'] : null; ?>"/>
				<button class="rpr-ing-add-link button dashicons dashicons-admin-links <?php echo $has_link; ?>"
					  data-type="rpr_ingredient"
					  title="<?php echo $has_link ? ( '' !== $ing['link'] ? $ing['link'] : $global_link ) : __( 'Add custom link', 'recipepress-reloaded' ); ?>"></button>
				<button class="rpr-ing-del-link button dashicons dashicons-editor-unlink <?php echo ( '' === $has_link ) ? 'rpr-hidden' : ''; ?>"
					  data-type="rpr_ingredient" title="<?php _e( 'Remove custom link', 'recipepress-reloaded' ) ?>"></button>
			</td>
			<td class="rpr-ing-add">
				<button class="rpr-ing-add-row button dashicons dashicons-plus" data-type="rpr_ingredient"
				   title="<?php esc_attr_e( 'Add ingredient', 'recipepress-reloaded' ) ?>"></button>
			</td>
			<td class="rpr-ing-del">
				<button class="rpr-ing-remove-row button dashicons dashicons-no" data-type="rpr_ingredient"
				   title="<?php esc_attr_e( 'Remove ingredient', 'recipepress-reloaded' ) ?>"></button>
			</td>
			</tr>
			<?php
			}
			$i ++;
		}
	} else { ?>
		<?php for ($x = 1; $x <= 3; $x++) { ?>
			<tr class="rpr-ing-row row-<?php echo $x; ?>">
				<td class="rpr-ing-sort">
					<div class="sort-handle dashicons dashicons-sort"></div>
					<input type="hidden" name="rpr_recipe_ingredients[<?php echo $x; ?>][sort]" class="ingredients_sort"
						   id="ingredients_sort_<?php echo $x; ?>"/>
				</td>
				<td class="rpr-ing-amount">
					<input type="text" name="rpr_recipe_ingredients[<?php echo $x; ?>][amount]" class="ingredients_amount"
						   id="ingredients_amount_<?php echo $x; ?>" placeholder="1"/>
				</td>
				<td class="rpr-ing-unit">
					<?php if ( Options::get_option( 'rpr_use_ingredient_unit_list' ) ) { ?>
						<select name="rpr_recipe_ingredients[<?php echo $x; ?>][unit]" class="ingredients_unit"
								id="ingredient_unit_<?php echo $x; ?>">
							<?php echo $this->get_the_ingredient_unit_selection(); ?>
						</select>
					<?php } else { ?>
						<input type="text" name="rpr_recipe_ingredients[<?php echo $x; ?>][unit]" class="ingredients_unit"
							   id="ingredient_unit_<?php echo $x; ?>" value=""
							   placeholder="<?php _e( 'teaspoon', 'recipepress-reloaded' ); ?>"/>
					<?php } ?>
				</td>
				<td class="rpr-ing-name">
					<input type="text" class="rpr-ing-name-input" name="rpr_recipe_ingredients[<?php echo $x; ?>][ingredient]"
						   class="ingredients_name" id="ingredients_<?php echo $x; ?>"
						   placeholder="<?php _e( 'olive oil', 'recipepress-reloaded' ); ?>" />
				</td>
				<td class="rpr-ing-note">
					<input type="text" name="rpr_recipe_ingredients[<?php echo $x; ?>][notes]" class="ingredients_notes"
						   id="ingredient_notes_<?php echo $x; ?>"
						   placeholder="<?php _e( 'extra virgin', 'recipepress-reloaded' ); ?>"/>
				</td>
				<td class="rpr-ing-link">
					<input name="rpr_recipe_ingredients[<?php echo $x; ?>][link]" class="rpr_recipe_ingredients_link"
						   type="hidden" id="ingredient_link_<?php echo $x; ?>" value=""/>
					<input name="rpr_recipe_ingredients[<?php echo $x; ?>][target]" class="rpr_recipe_ingredients_target"
						   type="hidden" id="ingredient_target_<?php echo $x; ?>" value=""/>
					<button href="#" class="rpr-ing-add-link button dashicons dashicons-admin-links" data-type="rpr_ingredient"
							title="<?php _e( 'Add custom link', 'recipepress-reloaded' ); ?>"></button>
					<button href="#" class="rpr-ing-del-link button dashicons dashicons-editor-unlink rpr-hidden" data-type="rpr_ingredient"
							title="<?php _e( 'Remove custom link', 'recipepress-reloaded' ); ?>"></button>
				</td>
				<td class="rpr-ing-add">
					<button class="rpr-ing-add-row button dashicons dashicons-plus" data-type="rpr_ingredient"
							title="<?php _e( 'Add ingredient', 'recipepress-reloaded' ) ?>"></button>
				</td>
				<td class="rpr-ing-del">
					<button class="rpr-ing-remove-row button dashicons dashicons-no" data-type="rpr_ingredient"
							title="<?php _e( 'Remove ingredient', 'recipepress-reloaded' ) ?>"></button>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>

	<?php if ( is_array( $ingredients ) ) : ?>
	<!-- the last row is always empty, in case you want to add some -->
	<tr class="rpr-ing-row row-<?php echo $i; ?>">
		<td class="rpr-ing-sort">
			<div class="sort-handle dashicons dashicons-sort"></div>
			<input type="hidden" name="rpr_recipe_ingredients[<?php echo $i; ?>][sort]" class="ingredients_sort"
				   id="ingredients_sort_<?php echo $i; ?>"/>
		</td>
		<td class="rpr-ing-amount">
			<input type="text" name="rpr_recipe_ingredients[<?php echo $i; ?>][amount]" class="ingredients_amount"
				   id="ingredients_amount_<?php echo $i; ?>" placeholder="1"/>
		</td>
		<td class="rpr-ing-unit">
			<?php if ( Options::get_option( 'rpr_use_ingredient_unit_list' ) ) { ?>
				<select name="rpr_recipe_ingredients[<?php echo $i; ?>][unit]" class="ingredients_unit"
						id="ingredient_unit_<?php echo $i; ?>">
					<?php echo $this->get_the_ingredient_unit_selection(); ?>
				</select>
			<?php } else { ?>
				<input type="text" name="rpr_recipe_ingredients[<?php echo $i; ?>][unit]" class="ingredients_unit"
					   id="ingredient_unit_<?php echo $i; ?>" value=""
					   placeholder="<?php _e( 'teaspoon', 'recipepress-reloaded' ); ?>"/>
			<?php } ?>
		</td>
		<td class="rpr-ing-name">
			<input type="text" class="rpr-ing-name-input" name="rpr_recipe_ingredients[<?php echo $i; ?>][ingredient]"
				   class="ingredients_name" id="ingredients_<?php echo $i; ?>"
				   placeholder="<?php _e( 'olive oil', 'recipepress-reloaded' ); ?>" />
		</td>
		<td class="rpr-ing-note">
			<input type="text" name="rpr_recipe_ingredients[<?php echo $i; ?>][notes]" class="ingredients_notes"
				   id="ingredient_notes_<?php echo $i; ?>"
				   placeholder="<?php _e( 'extra virgin', 'recipepress-reloaded' ); ?>"/>
		</td>
		<td class="rpr-ing-link">
			<input name="rpr_recipe_ingredients[<?php echo $i; ?>][link]" class="rpr_recipe_ingredients_link"
				   type="hidden" id="ingredient_link_<?php echo $i; ?>" value=""/>
			<input name="rpr_recipe_ingredients[<?php echo $i; ?>][target]" class="rpr_recipe_ingredients_target"
				   type="hidden" id="ingredient_target_<?php echo $i; ?>" value=""/>
			<button href="#" class="rpr-ing-add-link button dashicons dashicons-admin-links" data-type="rpr_ingredient"
					title="<?php _e( 'Add custom link', 'recipepress-reloaded' ); ?>"></button>
			<button href="#" class="rpr-ing-del-link button dashicons dashicons-editor-unlink rpr-hidden" data-type="rpr_ingredient"
					title="<?php _e( 'Remove custom link', 'recipepress-reloaded' ); ?>"></button> <!-- @TODO: add back CSS class 'rpr-hidden' -->
		</td>
		<td class="rpr-ing-add">
			<button class="rpr-ing-add-row button dashicons dashicons-plus" data-type="rpr_ingredient"
					title="<?php _e( 'Add ingredient', 'recipepress-reloaded' ) ?>"></button>
		</td>
		<td class="rpr-ing-del">
			<button class="rpr-ing-remove-row button dashicons dashicons-no" data-type="rpr_ingredient"
					title="<?php _e( 'Remove ingredient', 'recipepress-reloaded' ) ?>"></button>
		</td>
	</tr>
	<?php endif; ?>

	</tbody>
	<tfoot>
	<tr>
		<td colspan="7" style="padding: 3em 0 0 0;">
			<button id="rpr-ing-add-row-ing" class="rpr rpr-ing-add-row button" data-type="rpr_ingredient" href="#">
				<span class="dashicons dashicons-plus"></span>
				<?php _e( 'Add an ingredient', 'recipepress-reloaded' ); ?>
			</button>
			<button id="rpr-ing-add-row-grp" class="rpr rpr-ing-add-row button" data-type="heading" href="#">
				<span class="dashicons dashicons-plus"></span>
				<?php _e( 'Add a group', 'recipepress-reloaded' ); ?>
			</button>
		</td>
	</tr>
	</tfoot>
</table>

