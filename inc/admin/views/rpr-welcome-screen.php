<div class="wrap rpr welcome-screen">
	<section class="rpr welcome-screen header">
		<div style="width: 150px;height: 150px;float: right;display: inline;background-color: #ccc;margin: 0 0 0 1em;"></div>
		<div>
			<h1>Welcome to Recipepress Reloaded</h1>
			<p>Enthusiastically enhance resource-leveling web-readiness before top-line schemas. Competently enhance stand-alone schemas whereas next-generation process improvements Efficiently e-enable sustainable expertise with diverse experiences.</p>
			<p>Holisticly integrate prospective processes and dynamic niches. Energistically monetize market-driven web services via intermandated results.</p>
		</div>
	</section>
	<section class="rpr welcome-screen body">
		<p>Enthusiastically enhance resource-leveling web-readiness before top-line schemas. Competently enhance stand-alone schemas whereas next-generation process improvements Efficiently e-enable sustainable expertise with diverse experiences.</p>
		<p>Holisticly integrate prospective processes and dynamic niches. Energistically monetize market-driven web services via intermandated results.</p>
	</section>
</div>

<!--<div class="glsr-welcome wrap about-wrap about-wrap-content">
	<h1>Welcome to Site Reviews</h1>
	<div class="about-text">Site Reviews is a free WordPress review plugin with advanced features that makes it easy to manage reviews on your website. Follow the instructions below to get started!</div>
	<div class="wp-badge">Version 4.2.9</div>
	<p class="about-buttons">
		<a class="button" href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation">Documentation</a>
		<a class="button" href="https://wordpress.org/support/plugin/site-reviews/">Support</a>
		<a target="_blank" class="button" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//geminilabs.io/site-reviews"><span class="dashicons dashicons-facebook-alt"></span> Share</a>
		<a target="_blank" class="button" href="https://twitter.com/intent/tweet?text=Site Reviews is a fantastic WordPress review plugin with advanced features that makes it easy to manage reviews on your website.&amp;url=https://geminilabs.io/site-reviews&amp;hashtags=WordPress,reviewplugins,"><span class="dashicons dashicons-twitter"></span> Tweet</a>
	</p>
	<nav class="glsr-nav-tab-wrapper nav-tab-wrapper">
		<a class="glsr-nav-tab nav-tab nav-tab-active" href="#getting-started">Getting Started</a>
		<a class="glsr-nav-tab nav-tab" href="#whatsnew">What's New</a>
		<a class="glsr-nav-tab nav-tab" href="#upgrade-guide">Upgrade Guide</a>
		<a class="glsr-nav-tab nav-tab" href="#support">Support</a>
	</nav>
	<div class="glsr-nav-view" id="getting-started">
		<div class="is-fullwidth">
			<div class="glsr-flex-row glsr-has-2-columns">
				<div class="glsr-column">
					<h3>Editor Blocks</h3>
					<p>The fastest way to getting started with Site Reviews is use the three provided blocks in the WordPress Block Editor. Each block comes with multiple settings which let you configure the block exactly as needed. To add a block to your page, click the "Add Block" button and search for "Site Reviews".</p>
					<img class="screenshot" src="http://rcno.local/wp-content/plugins/site-reviews/assets/images/blocks.png" alt="Editor Blocks">
				</div>
				<div class="glsr-column">
					<h3>Shortcodes and Widgets</h3>
					<p>You can also use the shortcodes or widgets on your page. Keep in mind, however, that widgets are limited in options compared to the shortcodes (for example, the "Latest Reviews" widget does not allow pagination). If you are using the Classic Editor in WordPress, you can click on the Site Reviews shortcode button above the editor (next to the media button) to add a shortcode via a friendly popup.</p>
					<p>To learn more about the shortcodes and the available shortcode options, please see the Shortcode Documentation page of the plugin.</p>
					<a class="button" href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!shortcodes">View Shortcode Documentation</a>
				</div>
			</div>
		</div>
		<hr>
		<div class="is-fullwidth">
			<h2>Features</h2>
			<ul class="glsr-flex-row glsr-has-3-columns">
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=addons">Add-ons</a></h3>
					<p>Extend Site Reviews with add-ons that provide additional features.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!reviews">Avatars</a></h3>
					<p>Enable avatars to generate images using the WordPress Gravatar service.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=tools#!general">Backup/Restore</a></h3>
					<p>Backup and restore your plugin settings as needed.</p>
				</li>
				<li class="glsr-column">
					<h3><a data-expand="#faq-14" href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!faq">Bayesian Ranking</a></h3>
					<p>Easily rank pages with assigned reviews using the bayesian algorithm.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!submissions">Blacklist</a></h3>
					<p>Blacklist words, phrases, IP addresses, names, and emails.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!general">Blockchain Validation</a></h3>
					<p>Verify your reviews on the Blockchain with <a href="https://rebusify.com?ref=105">Rebusify</a>.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit-tags.php?taxonomy=site-review-category&amp;post_type=site-review">Categories</a></h3>
					<p>Add your own categories and assign reviews to them.</p>
				</li>
				<li class="glsr-column">
					<h3><a target="_blank" href="https://github.com/pryley/site-reviews/blob/master/HOOKS.md">Developer Friendly</a></h3>
					<p>Designed for WordPress developers with over 100 filter hooks and convenient functions.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!faq">Documentation</a></h3>
					<p>Provides FAQ and documenation for hooks and all shortcodes and functions.</p>
				</li>
				<li class="glsr-column">
					<h3><a target="_blank" href="https://wordpress.org/support/article/adding-a-new-block/">Editor Blocks</a></h3>
					<p>Use configurable editor blocks in the new WordPress 5.0 editor.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!schema">JSON-LD Schema</a></h3>
					<p>Enable JSON-LD schema to display your reviews and ratings in search results.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!general">Multilingual</a></h3>
					<p>Integrates with Polylang and WPML and provides easy search/replace translation.</p>
				</li>
				<li class="glsr-column">
					<h3><a target="_blank" href="https://wordpress.org/support/article/create-a-network/">Multisite Support</a></h3>
					<p>Provides full support for the WordPress multisite feature.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!general">Notifications</a></h3>
					<p>Send notifications to one or more emails when a review is submitted.</p>
				</li>
				<li class="glsr-column">
					<h3><a data-expand="#faq-03" href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!faq">Page Assignment</a></h3>
					<p>Assign reviews to Posts, Pages, and Custom Post Types.</p>
				</li>
				<li class="glsr-column">
					<h3><a data-expand="#faq-02" href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!faq">Pagination</a></h3>
					<p>Enable AJAX pagination to display a custom number of reviews per-page.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review">Responses</a></h3>
					<p>Write a response to reviews that require a response.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!general">Restrictions</a></h3>
					<p>Require approval before publishing reviews and limit to registered users.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!submissions">Review Limits</a></h3>
					<p>Limit review submissions by email address, IP address, or username.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!shortcodes">Review Summaries</a></h3>
					<p>Display a summary of your review ratings from high to low.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!shortcodes">Shortcodes</a></h3>
					<p>Use the configurable shortcodes complete with documentation.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!general">Slack</a></h3>
					<p>Receive notifications in Slack when a review is submitted.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!submissions">SPAM Protection</a></h3>
					<p>Uses a Honeypot and integrates with Invisible reCAPTCHA and Akismet.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=settings#!general">Styles</a></h3>
					<p>Change the submission form style to match popular themes and form plugins.</p>
				</li>
				<li class="glsr-column">
					<h3><a target="_blank" href="https://wordpress.org/support/plugin/site-reviews/">Support</a></h3>
					<p>Free premium-level support included on the WordPress support forum.</p>
				</li>
				<li class="glsr-column">
					<h3><a data-expand="#faq-17" href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!faq">Templates</a></h3>
					<p>Use the Site Reviews templates in your theme for full control over the HTML.</p>
				</li>
				<li class="glsr-column">
					<h3><a href="http://rcno.local/wp-admin/widgets.php">Widgets</a></h3>
					<p>Use the configurable widgets in your sidebars.</p>
				</li>
			</ul>
		</div>    </div>
	<div class="glsr-nav-view ui-tabs-hide" id="whatsnew">
		<p class="about-description">We think you'll love the changes in this new release!</p>
		<div class="is-fullwidth">
			<div class="glsr-flex-row">
				<div class="glsr-column">
					<div class="card is-fullwidth">
						<h3>4.2.0</h3>
						<p><em>Release Date — October 29th, 2019</em></p>
						<h4>New Features</h4>
						<ul>
							<li>Added the <code>site-reviews/support/deprecated/v4</code> filter hook. If this hook returns <code>false</code> then the plugin will skip deprecated checks. If the plugin console does not show any deprecated notices, then it should be safe to use this hook for increased performance.</li>
							<li>Added WordPress v5.3 compatibility</li>
						</ul>
						<h4>Tweaks</h4>
						<ul>
							<li>Optimised translation performance</li>
							<li>Rebuilt the WordPress Editor Blocks</li>
						</ul>
						<h4>Bugs Fixed</h4>
						<ul>
							<li>Fixed pagination of reviews on static front page</li>
							<li>Fixed performance issues related to IP Address detection</li>
							<li>Fixed potential SSL error when fetching Cloudflare IP ranges</li>
							<li>Fixed System Info when ini_get() function is disabled</li>
						</ul>
					</div>
					<div class="card is-fullwidth">
						<h3>4.1.0</h3>
						<p><em>Release Date — October 16th, 2019</em></p>
						<h4>New Features</h4>
						<ul>
							<li>Added optional "Email", "IP Address", and "Response" columns to the reviews table</li>
						</ul>
						<h4>Changes</h4>
						<ul>
							<li>Changed <code>[site_reviews]</code> "count" option name to "display" (i.e. [site_reviews display=10])</li>
							<li>Changed <code>glsr_get_reviews()</code> "count" option name to "per_page" (i.e. glsr_get_reviews(['per_page' =&gt; 10]))</li>
							<li>Renamed "Documentation" page to "Help"</li>
						</ul>
						<h4>Tweaks</h4>
						<ul>
							<li>Updated the "Common Problems and Solutions" help section</li>
						</ul>
						<h4>Bugs Fixed</h4>
						<ul>
							<li>Fixed a HTML5 validation issue in the plugin settings</li>
							<li>Fixed column sorting on the reviews table</li>
							<li>Fixed email template tags</li>
							<li>Fixed IP address detection for servers that do not support IPv6</li>
							<li>Fixed pagination links from triggering in the editor block</li>
							<li>Fixed pagination when using the default count of 5 reviews per page</li>
							<li>Fixed pagination with hidden review fields</li>
							<li>Fixed PHP compatibility issues</li>
							<li>Fixed plugin migration on update</li>
							<li>Fixed plugin uninstall</li>
							<li>Fixed translations for default text that include a HTML link</li>
						</ul>
					</div>
					<div class="card is-fullwidth">
						<h3>4.0.0</h3>
						<p><em>Release Date — October 6th, 2019</em></p>
						<h4>New Features</h4>
						<ul>
							<li>Added Multisite support</li>
							<li>Added product schema price options</li>
							<li>Added proxy header support for IP detection</li>
							<li>Added <a href="https://rebusify.com?ref=105" target="_blank">Rebusify Confidence System</a> integration for blockchain verification of reviews</li>
							<li>Added setting to choose the name format of the review author</li>
							<li>Added setting to choose which blacklist to use</li>
							<li>Added setting to limit review submissions</li>
							<li>Added widget icons in the WordPress customizer</li>
							<li>Added WPML integration for summary counts</li>
						</ul>
						<h4>Changes</h4>
						<ul>
							<li>Changed category assignment to one-per-review</li>
							<li>Removed $_SESSION usage</li>
						</ul>
						<h4>Tweaks</h4>
						<ul>
							<li>Improved AJAX pagination</li>
							<li>Improved documentation</li>
							<li>Improved email failure logging</li>
							<li>Improved internal console usage</li>
							<li>Improved system info</li>
							<li>Updated FAQs</li>
							<li>Updated plugin hooks</li>
							<li>Updated templates</li>
						</ul>
						<h4>Bugs Fixed</h4>
						<ul>
							<li>Fixed badge counter in menu when reviews are approved/unapproved</li>
							<li>Fixed overriding star styles on the "Add plugin" page</li>
							<li>Fixed per-page limit in the Reviews block</li>
							<li>Fixed PHP 7.2 support</li>
							<li>Fixed review counts</li>
							<li>Fixed review menu counts from changing when approving/unapproving comments</li>
							<li>Fixed review revert button</li>
							<li>Fixed star-rating CSS when using the helper function</li>
							<li>Fixed upgrade process when updating to a major plugin version</li>
						</ul>
					</div>
				</div>
			</div>
		</div>    </div>
	<div class="glsr-nav-view ui-tabs-hide" id="upgrade-guide">
		<p class="about-description">If you are using any Site Reviews code snippets or have built any custom integrations with Site Reviews, please make sure to read this upgrade guide.</p>
		<div class="is-fullwidth">
			<div class="glsr-flex-row">
				<div class="glsr-column">
					<div class="glsr-card card is-fullwidth">
						<h3>Meta Keys</h3>
						<p><em>Likelihood Of Impact: Low</em></p>
						<p>The Meta Keys that Site Reviews 4.0 uses to store information to reviews are now protected (they begin with an underscore) so that they don't show up in the Custom Fields Meta Box on other pages. This change can potentially affect you if one of the following cases apply:</p>
						<ul>
							<li>You are using the <code>WP_Query</code> class or the <code>get_posts</code> function to fetch reviews and are setting the <code>meta_key</code> or <code>meta_query</code> options</li>
							<li>You are using the <code>pre_get_posts</code> WordPress filter to modify the Site Reviews <code>meta_query</code></li>
							<li>You are using the <code>update_postmeta</code> WordPress filter with reviews and are checking the <code>meta_key</code> value</li>
						</ul>
						<p>If any of the above cases apply to you, please make sure that you prefix the Meta Keys with an underscore.</p>
						<p>Here is a <code>get_posts</code> example:</p>
						<pre><code class="php">// OLD CODE
$reviews = get_posts([
    'meta_query' =&gt; [['key' =&gt; 'assigned_to', 'value' =&gt; 123]],
    'post_type' =&gt; 'site-review',
]);

// NEW CODE:
$reviews = get_posts([
    'meta_query' =&gt; [['key' =&gt; '_assigned_to', 'value' =&gt; 123]], &lt;-- here we changed the meta_key
    'post_type' =&gt; 'site-review',
]);
</code></pre>
						<p>Here is a <code>pre_get_posts</code> example:</p>
						<pre><code class="php">// OLD CODE
add_action('pre_get_posts', function ($query) {
    if ($query-&gt;get('post_type') == 'site-review') {
        $meta = (array) $query-&gt;get('meta_keyquery');
        $index = array_search('rating', array_column($meta, 'key'));
        // ...
    }
});

// NEW CODE
add_action('pre_get_posts', function ($query) {
    if ($query-&gt;get('post_type') == 'site-review') {
        $meta = (array) $query-&gt;get('meta_query');
        $index = array_search('_rating', array_column($meta, 'key')); &lt;-- here we changed the meta_key
        // ...
    }
});</code></pre>
						<p>Here is a <code>update_postmeta</code> example:</p>
						<pre><code class="php">// OLD CODE
add_action('update_postmeta', function ($metaId, $postId, $metaKey) {
    $review = apply_filters('glsr_get_review', null, $postId);
    if (!empty($review-&gt;ID) &amp;&amp; 'response' == $metaKey) {
        // ...
    }
}, 10, 3);

// NEW CODE
add_action('update_postmeta', function ($metaId, $postId, $metaKey) {
    $review = apply_filters('glsr_get_review', null, $postId);
    if (!empty($review-&gt;ID) &amp;&amp; '_response' == $metaKey) { &lt;-- here we changed the meta_key
        // ...
    }
}, 10, 3);</code></pre>
						<ul>
						</ul></div>
				</div>
			</div>
		</div>    </div>
	<div class="glsr-nav-view ui-tabs-hide" id="support">
		<p class="about-description">
			Still need help with Site Reviews? We offer excellent support for you. But don't forget to check our <a href="http://rcno.local/wp-admin/edit.php?post_type=site-review&amp;page=documentation#!shortcodes">documentation</a> first.
		</p>
		<div class="is-fullwidth">
			<div class="glsr-flex-row glsr-has-2-columns">
				<div class="glsr-column">
					<h3>Free Support</h3>
					<p>If you have any question about how to use the plugin, please open a new topic on the WordPress support forum. We will try to answer as soon as we can.</p>
					<p><a target="_blank" class="button" href="https://wordpress.org/support/plugin/site-reviews">Go to the Support Forum →</a></p>
				</div>
			</div>
		</div>    </div>
	<input type="hidden" name="_active_tab" value="getting-started">
	<input type="hidden" name="_wp_http_referer" value="http://rcno.local/wp-admin/plugins.php?plugin_status=all&amp;paged=1&amp;s#!getting-started">
</div>-->

<style>
	.rpr.welcome-screen {
		margin: 3em 0 0 0;
		background-color: #fff;
		padding: 2em;
	}
</style>
