(function( $ ) {
    'use strict';

    $(function() {

        var appID  = '7cbd246f';
        var appKey = 'eec01b95e813c9cdd82f74bcddbafc79';
        var url    = 'https://api.edamam.com/api/nutrition-details?app_id=' + appID + '&app_key=' + appKey;

        var recipe      = null;
        var ingredients = [];
        var recipeYield = $('#rpr_recipe_servings').val().trim() + ' ' + $('#rpr_recipe_servings_type').val().trim();
        var recipeTitle = $('#titlewrap #title').val().trim();

        $('table#recipe-ingredients tr.rpr-ing-row').each(function () {

            var row = $(this);

            row.each(function () {
                var item  = $(this), ingredient = [];
                var amount = item.find('.ingredients_amount').val().trim();
                var unit   = item.find('.ingredients_unit').val().trim();
                var name   = item.find('.rpr-ing-name-input').val().trim();
                var note   = item.find('.ingredients_notes').val().trim();

                ingredient.push(amount, unit, name);
                ingredients.push(ingredient.join(' '));
            });
        });

        recipe = {
            title: recipeTitle,
            ingr: ingredients,
            yield: recipeYield
        };

        // Create CORS request.
        function createCORSRequest(method, url) {
            var xhr = new XMLHttpRequest();
            if ("withCredentials" in xhr) {
                // XHR for Chrome/Firefox/Opera/Safari.
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest != "undefined") {
                // XDomainRequest for IE.
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                // CORS not supported.
                xhr = null;
            }
            return xhr;
        }

        // Make the actual CORS request.
        function makeCorsRequest() {

            var xhr = createCORSRequest('POST', url);

            if (!xhr) {
                console.log('CORS not supported');
                return;
            }

            // Response handlers.
            xhr.onload = function() {
                console.log( xhr.responseText );
            };

            xhr.onerror = function() {
                console.log('Whoops, there was an error making the request.');
            };

            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send( JSON.stringify(recipe) );
        }

        // makeCorsRequest();

    });

})( jQuery );
