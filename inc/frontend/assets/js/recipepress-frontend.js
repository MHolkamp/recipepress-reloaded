function socialShare(elem) {
	jQuery.ajax({
		type: 'post',
		url: rpr_public_vars.ajax_url,
		data: {
			action: 'save_share_count',
			social_site: elem.parentNode.getAttribute('data-social'),
			share_nonce: rpr_public_vars.share_nonce,
			recipe_id: rpr_public_vars.recipe_id,
		}
	});
}

(function( $ ) {
	'use strict';

	$(function() {

		/*$('.rpr-ingredients-container .rpr-ingredient').each(function () {
			// convert_units( $(this) ); // $(this) == each ingredient line.
		});*/

		function convert_units($ing) {

			// Find the `.rpr-ingredient-quantity` span then read its `data-ingredient-quantity` attribute.
			var qnty = $ing.find('.rpr-ingredient-quantity').data('ingredient-quantity');
			// Find the `.rpr-ingredient-unit` span then read its `data-ingredient-unit` attribute.
			var unit = $ing.find('.rpr-ingredient-unit').data('ingredient-unit');

			if ( typeof unit !== 'undefined' ) {

				console.log(unit);

				if ( unit === 'cup' || unit === 'cups' ) {
					$ing.find( '.rpr-ingredient-quantity' ).text( Math.ceil( qnty * 2 ) );
					$ing.find( '.rpr-ingredient-unit' ).text( 'mg' );
				}

				if ( unit === 'tsp' || unit === 'teaspoon' || unit === 'teaspoons' ) {
					$ing.find( '.rpr-ingredient-quantity' ).text( Math.ceil( qnty * 1.45 ) );
					$ing.find( '.rpr-ingredient-unit' ).text( 'g' );
				}

				if ( unit === 'tbsp' || unit === 'tablespoon' || unit === 'tablespoons' ) {
					$ing.find( '.rpr-ingredient-quantity' ).text( Math.ceil( qnty * 1.45 ) );
					$ing.find( '.rpr-ingredient-unit' ).text( 'g' );
				}

			}

		}

		$('.rpr-filter-section input').click(function () {
			$('form#rpr-recipe-archive-filter').submit();
		});

	});

})( jQuery );
