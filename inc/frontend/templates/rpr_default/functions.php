<?php
/**
 * Registers the stylesheet for the default template.
 *
 * @package Recipepress
 *
 * @uses wp_enqueue_style
 * @return void
 */

use Recipepress\Inc\Frontend\Template;
use Recipepress\Inc\Core\Options;

// Enqueue our template specific script and style.
add_action( 'wp_enqueue_scripts', 'rpr_default_styles' );
add_action( 'wp_enqueue_scripts', 'rpr_default_scripts' );

// If we're on a single recipe post, add the JSON-LD data to the HTML <head> tag.
// Otherwise, it will be added to the <body> via the 'recipe.php' file.
add_action( 'wp_head', 'rpr_add_json_to_head' );

/**
 * Loads a CSS file attached to this template, for convenience.
 */
function rpr_default_styles() {

	wp_enqueue_style( 'rpr-default-template-style', plugin_dir_url( __FILE__ ) . 'assets/rpr-default.css', array(), '1.0.0', 'all' );

	if ( Options::get_option( 'rpr_recipe_template_print_btn' ) ) {
		wp_enqueue_style( 'rpr-default-print-style', plugin_dir_url( __FILE__ ) . 'assets/print.css', array(), '1.0.0', 'print' );
	}
}


/**
 * Loads a JS file attached to this template, for convenience.
 */
function rpr_default_scripts() {

	wp_enqueue_script( 'rpr-default-template-script', plugin_dir_url( __FILE__ ) . 'assets/rpr-default.js', array( 'jquery' ), '1.0.0', true );
	wp_localize_script(
		'rpr-default-template-script',
		'print_options',
		array(
			'print_area'    => Options::get_option( 'rpr_recipe_template_print_area' ),
			'no_print_area' => Options::get_option( 'rpr_recipe_template_no_print_area' ),
			'print_css'     => plugin_dir_url( __FILE__ ) . 'assets/print.css',
		)
	);

}

/**
 * Get our schema data from the Template class and echo it for convenience.
 */
function recipe_schema() {

	if ( ! is_singular( 'rpr_recipe' ) ) {
		return null;
	}

	$recipe_id = ( isset( $GLOBALS['recipe_id'] ) && '' !== $GLOBALS['recipe_id'] ) ? $GLOBALS['recipe_id'] : get_the_ID();
	$template  = new Template( '1.0.0', 'recipepress-reloaded' );

	echo wp_json_encode( $template->get_the_rpr_recipe_schema( $recipe_id ) );
}


/**
 * Adds the schema data to a script tag to be inserted in the HTML <head> tag via the `wp_head` action.
 */
function rpr_add_json_to_head() { ?>
	<script  id="rpr-recipe-schema" type="application/ld+json"><?php recipe_schema(); ?></script>
	<?php
}
