<?php
/*
Author: wzyMedia
Author Mail: kemory@wzymedia.com
Author URL: https://wzymedia.com
Layout Name: Rpr Default
Version: 1.1.0
Description: The default layout.
*/

/**
 * The recipe post object.
 *
 * @var \WP_Post $recipe The recipe post object.
 */

use Recipepress\Inc\Frontend\Template;

// Get the recipe ID.
$recipe_id   = ! empty( $GLOBALS['recipe_id'] ) ? (int) $GLOBALS['recipe_id'] : $recipe->ID;
$template    = new Template( $this->plugin_name, $this->version );
?>

<?php do_action( 'rpr_recipe_template_start' ); ?>

<div id="rpr-recipe" class="rpr-recipe-container">

	<?php if ( $template->is_recipe_embedded() ) : ?>
		<h2 class="rpr_title">
			<?php
				// Displaying the recipe title normally handled by the theme as post_title().
				// However, if the recipe is embedded, we need to do it here.
				echo esc_html( get_the_title( $recipe_id ) );
			?>
		</h2>
		<?php do_action( 'rpr_recipe_after_embedded_title' ); ?>
	<?php endif; ?>

	<div class="rpr-terms-container">
		<?php
			// I think it's always nice to have an overview of the taxonomies a recipe is
			// filed under at the top.
			$template->the_rpr_taxonomy_list( $recipe_id, 'icon-tags', true, ', ' );

			do_action( 'rpr_recipe_after_taxonomy_list' );
		?>
		<?php do_action( 'rpr_recipe_after_taxonomy_list' ); ?>
	</div>

	<?php if ( $template->get_the_recipe_print_button( '' ) || $template->get_the_rpr_recipe_jump_button( '' ) ) : ?>
		<div class="rpr-jump-print-container">
			<?php
				$template->the_rpr_recipe_jump_button( 'icon-down-open', 'ingredients' );
				$template->the_recipe_print_button( 'icon-print', 'rpr_recipe' );
			?>
		</div>
	<?php endif; ?>

	<div class="rpr-description-container">
		<?php
			// Display a description of the recipe as entered in the WP post editor,
			// there should always be one.
			$template->the_recipe_description( $recipe_id );
		?>
	</div>
	<?php do_action( 'rpr_recipe_after_description' ); ?>

	<?php if ( null !== $template->get_the_rpr_recipe_source( $recipe_id, 'icon-link' ) ) : ?>
		<div class="rpr-source-container">
			<?php
				// Display source/citation information if available.
				$template->the_rpr_recipe_source( $recipe_id, 'icon-link' );
			?>
		</div>
		<?php do_action( 'rpr_recipe_after_source' ); ?>
	<?php endif; ?>

	<div class="rpr-ingredients-container">
		<?php
			// Ingredients section of the recipe.
			// First: The headline.
			$template->the_rpr_recipe_headline( __( 'Ingredients', 'recipepress-reloaded' ), 'icon-shopping-basket' );

			// Second: Serving size/yield.
			if ( null !== $template->get_the_rpr_recipe_servings( $recipe_id, 'icon-chart-pie' ) ) {
				$template->the_rpr_recipe_servings( $recipe_id, 'icon-chart-pie' );
			}

			// Third: The ingredient list.
			$template->the_rpr_recipe_ingredients( $recipe_id, 'icon-circle', 'icon-circle-empty' );
		?>
		<?php do_action( 'rpr_recipe_after_ingredients' ); ?>
	</div>

	<?php if ( null !== $template->get_the_rpr_recipe_times( $recipe_id, array() ) ) : ?>
		<div class="rpr-times-container">
			<?php
				// Display the recipe times bar.
				$template->the_rpr_recipe_times( $recipe_id, array( 'icon-hourglass', 'icon-fire', 'icon-clock' ) );
			?>
		</div>
		<?php do_action( 'rpr_recipe_after_times' ); ?>
	<?php endif; ?>

	<div class="rpr-instruction-container">
		<?php
			// Instructions section of the recipe.
			// First: the headline.
			$template->the_rpr_recipe_headline( __( 'Instructions', 'recipepress-reloaded' ), 'icon-book' );

			// Second: the instructions list.
			$template->the_rpr_recipe_instructions( $recipe_id, 'icon-circle' );
		?>
		<?php do_action( 'rpr_recipe_after_instructions' ); ?>
	</div>

	<?php if ( null !== $template->get_the_rpr_recipe_notes( $recipe_id ) ) : ?>
		<div class="rpr-notes-container">
			<?php
				// Notes section of the recipe
				// First: the headline.
				$template->the_rpr_recipe_headline( __( 'Notes', 'recipepress-reloaded' ), 'icon-attach' );

				// Second: the actual notes.
				$template->the_rpr_recipe_notes( $recipe_id );
			?>
		</div>
		<?php do_action( 'rpr_recipe_after_notes' ); ?>
	<?php endif; ?>

	<?php if ( null !== $template->get_the_rpr_recipe_nutrition( $recipe_id ) ) : ?>
		<div class="rpr-nutrition-container">
			<?php
			// Display nutritional information if available.
			$template->the_rpr_recipe_nutrition( $recipe_id );
			?>
		</div>
		<?php do_action( 'rpr_recipe_after_nutrition' ); ?>
	<?php endif; ?>

	<?php
	if ( $template->is_recipe_embedded() ) {
		$template->the_rpr_recipe_schema( $recipe_id );
	}
	?>

</div>

<?php do_action( 'rpr_recipe_template_end' ); ?>

<?php echo '<!-- Recipepress Reloaded ' . Recipepress\PLUGIN_VERSION . ' -->'; ?>
